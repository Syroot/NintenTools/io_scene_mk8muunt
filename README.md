# io_scene_mk8muunt

This is a Blender add-on to import and export Mario Kart 8 Course Info BYAML files (those with "muunt" in their name).

![](https://gitlab.com/Syroot/io_scene_mk8muunt/raw/master/doc/readme/example.png)

S. the wiki for [help and more information](https://gitlab.com/Syroot/io_scene_mk8muunt/wikis).

## Deprecation Notice

**This project aswell as other NintenTools projects is no longer updated or maintained.**
The following known issues result from this:
- The add-on does not work with Blender 2.80+.
